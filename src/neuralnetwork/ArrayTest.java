package neuralnetwork;

public class ArrayTest {
	public static void main(String[] args) {
		int[] arr1 = new int[10];
		int[] arr2;
		
		for(int i=0;i<arr1.length;i++)
			arr1[i] = i;
		
		System.out.println();
		for(int i=0;i<arr1.length;i++)
			System.out.print(arr1[i]+" ");
		
		arr2 = new int[arr1.length];
		arr2 = arr1;
		/*for(int i=0;i<arr1.length;i++)
			arr2[i] = arr1[i];*/
		
		System.out.println();
		for(int i=0;i<arr2.length;i++)
			System.out.print(arr2[i]+" ");
		
		for(int i=0;i<arr1.length;i++){
			arr1[i] = arr1.length-i-1;
		}
		
		System.out.println();
		for(int i=0;i<arr1.length;i++)
			System.out.print(arr1[i]+" ");
		
		System.out.println();
		for(int i=0;i<arr2.length;i++)
			System.out.print(arr2[i]+" ");
		
		
	}
	
	
}
