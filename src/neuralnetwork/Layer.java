package neuralnetwork;
import java.util.ArrayList;
import java.util.List;

public class Layer {
	private List<Node> nodes = new ArrayList<Node>();

	public Layer(int numOfNodes, boolean hasBias) {
		
		for (int i = 0; i < numOfNodes; i++){
			if(hasBias)
				nodes.add(new Node(numOfNodes,0));
			else
				nodes.add(new Node(numOfNodes));
				
		}
	}
	
	public List<Node> getNodes(){
		return nodes;
	}

	public void setNode(int position, double value){
		nodes.get(position).setValue(value);
	}
	
	public int getNumOfNodes(){
		return nodes.size();
	}
	
	public Node getNode(int position){
		return nodes.get(position);
	}
}
