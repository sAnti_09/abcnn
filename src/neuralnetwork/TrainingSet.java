package neuralnetwork;

import java.util.ArrayList;
import java.util.List;

public class TrainingSet {
	private int numOfInput;
	private int numOfDesiredOutput;
	private List<TrainingElement> trainingElements;

	public TrainingSet(int numOfInput, int numOfDesiredOutput) {
		this.numOfInput = numOfInput;
		this.numOfDesiredOutput = numOfDesiredOutput;
		trainingElements = new ArrayList<TrainingElement>();
	}

	public void addElement(TrainingElement newElement) {
		if (newElement.getNumOfInput() == numOfInput
				&& newElement.getNumOfDesiredOutput() == numOfDesiredOutput)
			trainingElements.add(newElement);
		else
			System.err.println("Training element not compatible to "
					+ numOfInput + "x" + numOfDesiredOutput);
	}

	public void addSet(TrainingSet newSet) {
		List<TrainingElement> newElements = newSet.getTrainingElements();
		for (int i = 0; i < newElements.size(); i++)
			addElement(newElements.get(i));
	}

	public List<TrainingElement> getTrainingElements() {
		return trainingElements;
	}

	public int getNumOfInputs() {
		return numOfInput;
	}

	public int getNumOfOutputs() {
		return numOfDesiredOutput;
	}

	public int getSize() {
		return trainingElements.size();
	}
	
	public void clear(){
		trainingElements.clear();
	}
}
