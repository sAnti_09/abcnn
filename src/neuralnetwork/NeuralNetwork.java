package neuralnetwork;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;
import abc.ABC;

public class NeuralNetwork {
	public static int SIGMOID = 1;
	public static int RADIAL = 2;
	private TransferFunction transferFunction;
	private boolean hasBias = true;
	private String name;

	private List<Layer> layers = new ArrayList<Layer>();
	private List<Double> weights = new ArrayList<Double>();

	private TrainingSet netSet;
	private TrainingSet newSet;
	private boolean finishedTraining;
	private boolean finishedSaving;
	private boolean finishedTesting;

	public NeuralNetwork() {

		setTransferFunction(new TransferFunction(TransferFunction.SIGMOID));
	}

	public NeuralNetwork(boolean hasBias, int... nodesInLayers) {

		setTransferFunction(new TransferFunction(TransferFunction.SIGMOID));
		this.hasBias = hasBias;
		netSet = new TrainingSet(nodesInLayers[0],
				nodesInLayers[nodesInLayers.length - 1]);
		newSet = new TrainingSet(nodesInLayers[0],
				nodesInLayers[nodesInLayers.length - 1]);
		createNetwork(nodesInLayers);
	}

	private void proxyConstructor(boolean hasBias, int... nodesInLayers) {
		this.hasBias = hasBias;
		netSet = new TrainingSet(nodesInLayers[0],
				nodesInLayers[nodesInLayers.length - 1]);
		newSet = new TrainingSet(nodesInLayers[0],
				nodesInLayers[nodesInLayers.length - 1]);
		createNetwork(nodesInLayers);
	}

	private void createNetwork(int... nodesInLayers) {
		layers.clear();
		weights.clear();
		layers.add(new Layer(nodesInLayers[0], false));
		for (int i = 1; i < nodesInLayers.length - 1; i++)
			layers.add(new Layer(nodesInLayers[i], hasBias));
		layers.add(new Layer(nodesInLayers[nodesInLayers.length - 1], hasBias));

		for (int i = 0; i < layers.size() - 1; i++) {
			for (int j = 0; j < layers.get(i + 1).getNumOfNodes(); j++) {
				for (int k = 0; k < layers.get(i).getNumOfNodes(); k++) {
					weights.add(new Double(0));
				}
				if (layers.get(i + 1).getNode(j).hasBias())
					weights.add(new Double(0));
			}
		}
	}

	private void refreshNetwork() {
		double value;
		int ctr = 0;
		for (int i = 0; i < layers.size() - 1; i++) {
			for (int j = 0; j < layers.get(i + 1).getNumOfNodes(); j++) {
				value = 0;
				for (int k = 0; k < layers.get(i).getNumOfNodes(); k++) {
					value += layers.get(i).getNode(k).getValue()
							* weights.get(ctr++);
				}
				if (layers.get(i + 1).getNode(j).hasBias())
					value += weights.get(ctr++);

				value = transferFunction.execute(value);
				layers.get(i + 1).setNode(j, value);
			}
		}
	}

	public void setTransferFunction(TransferFunction transferFunction) {
		this.transferFunction = transferFunction;
	}

	public void test(TrainingSet trainSet) {
		finishedTesting = false;
		TrainingElement newElement;
		for (int i = 0; i < trainSet.getTrainingElements().size(); i++) {
			newElement = trainSet.getTrainingElements().get(i);
			setInputs(newElement.getInput());
			refreshNetwork();
			// printResults(i);
		}
		finishedTesting = true;
		
	}
	
	public boolean isFinishedTesting(){
		return finishedTesting;
	}

	public int test(double[] input) {
		int output;
		setInputs(input);
		refreshNetwork();
		output = layers.get(layers.size() - 1).getNode(0).getValue() < 0.5f ? 0
				: 1;
		return output;
	}

	private void setInputs(double[] input) {
		if (input.length == layers.get(0).getNumOfNodes()) {
			for (int i = 0; i < input.length; i++) {
				layers.get(0).setNode(i, input[i]);
			}
		} else {
			System.err.println("Invalid input format!!");
		}
	}

	private void printResults(int ctr) {
		System.out.print("\nTest subject no: " + (ctr + 1));

		System.out.print("\nInput: ");
		for (int i = 0; i < layers.get(0).getNumOfNodes(); i++)
			System.out.print(" " + layers.get(0).getNode(i).getValue() + ",");

		System.out.print("\nOutput: ");
		for (int i = 0; i < layers.get(layers.size() - 1).getNumOfNodes(); i++)
			System.out
					.print(" "
							+ (layers.get(layers.size() - 1).getNode(i)
									.getValue() < 0.5f ? 0 : 1));
		System.out.println();
	}

	public void train(TrainingSet trainSet) {
		finishedTraining = false;
		if (trainSet.getSize() == 0) {
			System.out.println("Empty training set");
			return;
		}
		finishedTraining = false;
		newSet = trainSet;
		netSet.addSet(newSet);
		ABC abc = new ABC(this, netSet, 7500, 50, -1, 1);
		abc.start();
		setWeights(abc.getBestFood());
		finishedTraining = true;
	}

	public boolean isFinishedTraining() {
		return finishedTraining;
	}

	public void train() {
		ABC abc = new ABC(this, netSet, 2500, 50, -1, 1);
		abc.start();
		setWeights(abc.getBestFood());
	}

	public double rmsError(TrainingSet trainSet) {
		TrainingElement newElement;
		double error = 0;
		int size = 0;
		for (int i = 0; i < trainSet.getTrainingElements().size(); i++) {
			newElement = trainSet.getTrainingElements().get(i);
			setInputs(newElement.getInput());
			refreshNetwork();
			error += calculateError(newElement.getDesiredOutput());
			size += newElement.getNumOfDesiredOutput()
					* newElement.getNumOfDesiredOutput();
		}
		// error /= trainSet.getTrainingElements().size();
		error = Math.sqrt(error / size);
		return error;
	}

	private double calculateError(double[] desiredOutput) {
		double error = 0;
		double temp;
		for (int i = 0; i < layers.get(layers.size() - 1).getNumOfNodes(); i++) {
			temp = desiredOutput[i]
					- (layers.get(layers.size() - 1).getNode(i).getValue() >= 0.5f ? 1
							: 0);
			error += temp * temp;
		}
		// error /= layers.get(layers.size() - 1).getNumOfNodes();

		return error;
	}

	public int getParameterSize() {
		return weights.size();
	}

	public void setWeights(double[] newWeights) {
		for (int i = 0; i < weights.size(); i++) {
			weights.set(i, newWeights[i]);
		}
	}

	public void saveNetwork() {
		JFileChooser chooser = new JFileChooser();
		int choice = chooser.showSaveDialog(null);
		if (choice == JFileChooser.APPROVE_OPTION)
			saveNetwork(chooser.getSelectedFile().getAbsolutePath());
	}

	public void saveNetwork(String path) {
		finishedSaving = false;
		File networkFile = new File("networks/" + path + ".net");
		try {
			FileWriter writer = new FileWriter(networkFile);
			writer.append(path + "\n");
			for (int i = 0; i < layers.size(); i++) {
				writer.append(layers.get(i).getNumOfNodes() + " ");
			}
			writer.append("\n" + hasBias + "\n");
			for (int i = 0; i < weights.size(); i++) {
				writer.append(weights.get(i) + " ");
			}

			writer.append("\nMetaData");

			List<TrainingElement> trainingElements = netSet
					.getTrainingElements();
			for (int i = 0; i < trainingElements.size(); i++) {
				TrainingElement te = trainingElements.get(i);
				writer.append("\n" + te.getDesiredOutput()[0] + "::");
				for (int j = 0; j < te.getNumOfInput(); j++)
					writer.append(te.getInput()[j] + " ");

			}

			writer.close();
			System.out.println("Successfully saved network");
		} catch (IOException e) {
			System.err.println("Failed to save network");
		}
		finishedSaving = true;
	}

	public boolean isFinishedSaving() {
		return finishedSaving;
	}

	public void loadNetwork() {
		JFileChooser chooser = new JFileChooser();
		int choice = chooser.showOpenDialog(null);
		if (choice == JFileChooser.APPROVE_OPTION)
			loadNetwork(chooser.getSelectedFile().getAbsolutePath());
	}

	public void loadNetwork(String path) {
		try {
			File file = new File("networks/" + path + ".net");
			Scanner scan = new Scanner(file);
			List<Double> tempWeights = new ArrayList<Double>();
			String line;
			String[] chunks;
			String[] tempDesc;
			double out;
			double[] descriptor;
			boolean finishLoadingNetwork = false;
			while (scan.hasNext()) {
				line = scan.nextLine();

				if (line.trim().equals("MetaData"))
					finishLoadingNetwork = true;
				else {
					if (!finishLoadingNetwork) {
						name = line;
						line = scan.nextLine();
						chunks = line.trim().split(" ");
						int[] nil = new int[chunks.length];
						for (int i = 0; i < nil.length; i++)
							nil[i] = Integer.parseInt(chunks[i]);
						line = scan.nextLine().trim();
						boolean bool = Boolean.parseBoolean(line);
						proxyConstructor(bool, nil);
						line = scan.nextLine();
						chunks = line.trim().split(" ");
						for (int i = 0; i < chunks.length; i++) {
							tempWeights.add(Double.parseDouble(chunks[i]));
						}
					} else {
						chunks = line.trim().split("::");
						out = Double.parseDouble(chunks[0]);
						tempDesc = chunks[1].split(" ");
						descriptor = new double[tempDesc.length];
						for (int i = 0; i < descriptor.length; i++)
							descriptor[i] = Double.parseDouble(tempDesc[i]);
						netSet.addElement(new TrainingElement(descriptor,
								new double[] { out }));
					}
				}

			}

			if (tempWeights.size() == weights.size() || weights.size() != 0) {
				for (int i = 0; i < weights.size(); i++)
					weights.set(i, tempWeights.get(i));
				System.out.println("Successfully loaded network");
			} else {
				System.err.println("Failed to load network");
			}
		} catch (FileNotFoundException e) {
			System.err.println("Failed to load network");
		}
	}
	
	public int getInputCount(){
		return layers.get(0).getNumOfNodes();
	}
	
	public int getHiddenCount(){
		return layers.get(1).getNumOfNodes();
	}

}
