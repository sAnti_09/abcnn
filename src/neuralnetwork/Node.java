package neuralnetwork;

public class Node {
	private double value;
	private boolean hasBias = false;
	
	public Node(double value){
		this.value = value;
	}
	
	public Node(double value,double bias){
		this.value = value;
		hasBias = true;
	}
	
	public void setValue(double value){
		this.value = value;
	}
	
	public double getValue(){
		return value;
	}
	
	public boolean hasBias(){
		return hasBias;
	}

}
