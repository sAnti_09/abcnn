package neuralnetwork;

public class TrainerMain extends Thread {

	private Thread t;
	private volatile int i;
	private volatile int j;

	public TrainerMain() {
		t = new Thread(this);
		t.start();
	//	run();
	}

	@Override
	public void run() {
		for (i = 9; i <= 9; i++)
			for (j = 5; j <= 6; j++) {
				System.out.println("inputs: " + i + "   hidden nodes: " + j);
				new Trainer(i, j);

			}
	}

	public static void main(String[] args) {
		new TrainerMain();
	}

}
