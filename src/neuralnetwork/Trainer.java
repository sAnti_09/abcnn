package neuralnetwork;

import java.awt.EventQueue;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Trainer extends Thread {
	private NeuralNetwork neural;
	private TrainingSet trainingSet;
	private boolean hasPopulated = false;
	private Thread t;
	private int n, h;
	private boolean finished;
	public Trainer(int n, int h) {
		this.n = n;
		this.h = h;
		t = new Thread(this);
		t.start();
		//run();
	}

	public void run() {
		finished = false;
		long t = System.currentTimeMillis();
		neural = new NeuralNetwork(true, n, h, 1);
		trainingSet = new TrainingSet(n, 1);
		populateSet(n);
		/*
		 * while (!hasPopulated) { }
		 */
		// System.out.println("Starting training");
		neural.train(trainingSet);
		/*
		 * while (!neural.isFinishedTraining()) { }
		 */
		// System.out.println("Finished training");
		neural.saveNetwork("c" + n + "h" + h);
		/*
		 * while (!neural.isFinishedSaving()) { }
		 */
		neural.test(trainingSet);
		/*
		 * while (!neural.isFinishedTesting()) { }
		 */
		System.out.println("---------------------------------------     "
				+ (System.currentTimeMillis() - t) + " ms");
		finished = true;
	}
	
	public boolean isFinished(){
		return finished;
	}

	private void populateSet(int n) {
	//	System.out.println("populating data set");
		File dataset = new File("dataset/dataset" + n + ".txt");
		Scanner scan;
		String line;
		double[] input;
		String[] chunk, chunk2;
		TrainingElement element;
		try {
			scan = new Scanner(dataset);
			while (scan.hasNext()) {
				line = scan.nextLine();
				chunk = line.trim().split("::");
				chunk2 = chunk[1].trim().split(" ");
				input = new double[chunk2.length];
				for (int i = 0; i < input.length; i++)
					input[i] = Double.parseDouble(chunk2[i]);
				element = new TrainingElement(input,
						new double[] { Double.parseDouble(chunk[0]) });
				trainingSet.addElement(element);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		hasPopulated = true;
		// System.out.println("Finished populating data set");
	}

	public static void main(String[] args) {
		List<Trainer> trainers = new ArrayList<Trainer>();
		
		for (int i = 9; i <= 20; i++)
			for (int j = 5; j <= i; j++) {
				new Trainer(i, j);
			}

	}

}
