package neuralnetwork;
public class TransferFunction {
	public final static int SIGMOID = 1;
	public final static int RADIAL = 2;

	private int type;

	public TransferFunction(int type) {
		this.type = type;
	}

	public double execute(double input) {
		double value = 0;

		switch (type) {
		case SIGMOID:value =  1/(1+Math.exp(-input));
			break;
		case RADIAL:
			System.out.println("Radial not yet supported");
		}

		return value;
	}

}
