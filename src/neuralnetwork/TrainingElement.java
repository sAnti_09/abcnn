package neuralnetwork;
public class TrainingElement {
	private double[] input;
	private double[] desiredOutput;

	public TrainingElement(double[] input, double[] desiredOutput) {
		this.input = input;
		this.desiredOutput = desiredOutput;
	}

	public double[] getInput() {
		return input;
	}

	public double[] getDesiredOutput() {
		return desiredOutput;
	}
	
	public int getNumOfInput(){
		return input.length;
	}
	
	public int getNumOfDesiredOutput(){
		return desiredOutput.length;
	}
}
