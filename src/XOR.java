import neuralnetwork.NeuralNetwork;
import neuralnetwork.TrainingElement;
import neuralnetwork.TrainingSet;
import neuralnetwork.TransferFunction;
import abc.ABC;

public class XOR {
	private NeuralNetwork nn;
	private ABC abc;

	public XOR() {
		long elapsedTime;

		nn = new NeuralNetwork(true, 2,20,10,1);
		nn.setTransferFunction(new TransferFunction(TransferFunction.SIGMOID));
		
		TrainingSet set = new TrainingSet(2, 1);
		set.addElement(new TrainingElement(new double[]{0,0},new double[]{0}));
		set.addElement(new TrainingElement(new double[]{0,1},new double[]{1}));
		set.addElement(new TrainingElement(new double[]{1,0},new double[]{1}));
		set.addElement(new TrainingElement(new double[]{1,1},new double[]{0}));
		elapsedTime = System.currentTimeMillis();

		nn.train(set);
		elapsedTime = System.currentTimeMillis() - elapsedTime;
		System.out.println("Training time = " + elapsedTime + "ms");

		System.out.println(nn.test(new double[] { 0, 0 }));
		System.out.println(nn.test(new double[] { 0, 1 }));
		System.out.println(nn.test(new double[] { 1, 0 }));
		System.out.println(nn.test(new double[] { 1, 1 }));
	}

	public static void main(String[] args) {
		new XOR();
	}
}
